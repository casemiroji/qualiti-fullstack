import mongoose from "mongoose";

const ShortenerSchema = mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    expired: { type: Boolean, default: false },
    expiredDate: Date,
    hash: { type: String, required: true },
    hits: { type: Number, default: 0 },
    link: { type: String, required: true },
    metadata: [mongoose.Schema.Types.Mixed],
    name: String,
  },
  {
    timestamp: true,
  }
);

const ShortenerModel = mongoose.model("shortener", ShortenerSchema);

export default ShortenerModel;
